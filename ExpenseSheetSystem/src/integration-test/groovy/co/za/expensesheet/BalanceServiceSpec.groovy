package co.za.expensesheet

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class BalanceServiceSpec extends Specification {

    BalanceService balanceService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Balance(...).save(flush: true, failOnError: true)
        //new Balance(...).save(flush: true, failOnError: true)
        //Balance balance = new Balance(...).save(flush: true, failOnError: true)
        //new Balance(...).save(flush: true, failOnError: true)
        //new Balance(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //balance.id
    }

    void "test get"() {
        setupData()

        expect:
        balanceService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Balance> balanceList = balanceService.list(max: 2, offset: 2)

        then:
        balanceList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        balanceService.count() == 5
    }

    void "test delete"() {
        Long balanceId = setupData()

        expect:
        balanceService.count() == 5

        when:
        balanceService.delete(balanceId)
        sessionFactory.currentSession.flush()

        then:
        balanceService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Balance balance = new Balance()
        balanceService.save(balance)

        then:
        balance.id != null
    }
}
