package co.za.expensesheet

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class BalanceTransactionServiceSpec extends Specification {

    BalanceTransactionService balanceTransactionService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new BalanceTransaction(...).save(flush: true, failOnError: true)
        //new BalanceTransaction(...).save(flush: true, failOnError: true)
        //BalanceTransaction balanceTransaction = new BalanceTransaction(...).save(flush: true, failOnError: true)
        //new BalanceTransaction(...).save(flush: true, failOnError: true)
        //new BalanceTransaction(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //balanceTransaction.id
    }

    void "test get"() {
        setupData()

        expect:
        balanceTransactionService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<BalanceTransaction> balanceTransactionList = balanceTransactionService.list(max: 2, offset: 2)

        then:
        balanceTransactionList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        balanceTransactionService.count() == 5
    }

    void "test delete"() {
        Long balanceTransactionId = setupData()

        expect:
        balanceTransactionService.count() == 5

        when:
        balanceTransactionService.delete(balanceTransactionId)
        sessionFactory.currentSession.flush()

        then:
        balanceTransactionService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        BalanceTransaction balanceTransaction = new BalanceTransaction()
        balanceTransactionService.save(balanceTransaction)

        then:
        balanceTransaction.id != null
    }
}
