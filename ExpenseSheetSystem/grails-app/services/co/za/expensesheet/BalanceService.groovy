package co.za.expensesheet

import grails.gorm.services.Service

@Service(Balance)
interface BalanceService {

    Balance get(Serializable id)

    List<Balance> list(Map args)

    Long count()

    void delete(Serializable id)

    Balance save(Balance balance)

}