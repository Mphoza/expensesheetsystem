package co.za.expensesheet

import grails.gorm.transactions.Transactional

@Transactional
class ExpenseSheetBusinessService {

    def serviceMethod() {
		
		println "test"
		
    }
    
    
    def registerUser(String userName, double initialBalanceAmount) {
		
		User user = new User();
    	user.name = userName

    	if(user.save(flush: true)){
    	
    	    Balance userBalance = new Balance()
    	    
    	    userBalance.user = user
    	    userBalance.balance = initialBalanceAmount
    	    userBalance.save()
    	    
    	}

    }

    
    def recordUserExpense(String userName, def amount){
    
    	BalanceTransaction balanceTransaction = new BalanceTransaction()
            
        def userBalance = getUserBalance(userName)
        def newBalance = userBalance.balance - amount
        balanceTransaction.balance = userBalance
        balanceTransaction.date = new Date()
        balanceTransaction.spentAmount = amount
        balanceTransaction.balanceAmount = newBalance
        if(balanceTransaction.save(flush: true)){
            userBalance.balance = newBalance
            userBalance.save()
        }      
    }
    
    def listUserTransactions(String userName){
            
         def lstTransaction = getUserTransactions(userName)
         return lstTransaction;
          
    }


    
    def getUserBalance(String userName){
    
    	def user = getUser(userName)
    	println user
    	def userBalance = Balance.findByUser(user)
    	
    	return userBalance
        
    }
    
     def getUserTransactions(String userName){
    
    	def userBalance = getUserBalance(userName)
    	def balanceTransaction = BalanceTransaction.findAllByBalance(userBalance)
    	
    	return balanceTransaction
        
    }
    
    def getUser(def userName){
        
        def user = User.findByName(userName);
        
        return user
        
    }



}
