package co.za.expensesheet

import grails.gorm.services.Service

@Service(BalanceTransaction)
interface BalanceTransactionService {

    BalanceTransaction get(Serializable id)

    List<BalanceTransaction> list(Map args)

    Long count()

    void delete(Serializable id)

    BalanceTransaction save(BalanceTransaction balanceTransaction)

}