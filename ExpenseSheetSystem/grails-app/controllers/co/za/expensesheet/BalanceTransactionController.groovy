package co.za.expensesheet

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class BalanceTransactionController {

	static scaffold = BalanceTransaction

    BalanceTransactionService balanceTransactionService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond balanceTransactionService.list(params), model:[balanceTransactionCount: balanceTransactionService.count()]
    }

    def show(Long id) {
        respond balanceTransactionService.get(id)
    }

    def create() {
        respond new BalanceTransaction(params)
    }

    def save(BalanceTransaction balanceTransaction) {
        if (balanceTransaction == null) {
            notFound()
            return
        }

        try {
            balanceTransactionService.save(balanceTransaction)
        } catch (ValidationException e) {
            respond balanceTransaction.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'balanceTransaction.label', default: 'BalanceTransaction'), balanceTransaction.id])
                redirect balanceTransaction
            }
            '*' { respond balanceTransaction, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond balanceTransactionService.get(id)
    }

    def update(BalanceTransaction balanceTransaction) {
        if (balanceTransaction == null) {
            notFound()
            return
        }

        try {
            balanceTransactionService.save(balanceTransaction)
        } catch (ValidationException e) {
            respond balanceTransaction.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'balanceTransaction.label', default: 'BalanceTransaction'), balanceTransaction.id])
                redirect balanceTransaction
            }
            '*'{ respond balanceTransaction, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        balanceTransactionService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'balanceTransaction.label', default: 'BalanceTransaction'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'balanceTransaction.label', default: 'BalanceTransaction'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
