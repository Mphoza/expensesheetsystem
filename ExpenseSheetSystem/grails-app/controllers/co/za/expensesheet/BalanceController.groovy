package co.za.expensesheet

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class BalanceController {

    BalanceService balanceService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond balanceService.list(params), model:[balanceCount: balanceService.count()]
    }

    def show(Long id) {
        respond balanceService.get(id)
    }

    def create() {
        respond new Balance(params)
    }

    def save(Balance balance) {
        if (balance == null) {
            notFound()
            return
        }

        try {
            balanceService.save(balance)
        } catch (ValidationException e) {
            respond balance.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'balance.label', default: 'Balance'), balance.id])
                redirect balance
            }
            '*' { respond balance, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond balanceService.get(id)
    }

    def update(Balance balance) {
        if (balance == null) {
            notFound()
            return
        }

        try {
            balanceService.save(balance)
        } catch (ValidationException e) {
            respond balance.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'balance.label', default: 'Balance'), balance.id])
                redirect balance
            }
            '*'{ respond balance, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        balanceService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'balance.label', default: 'Balance'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'balance.label', default: 'Balance'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
