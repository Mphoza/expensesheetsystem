package co.za.expensesheet

class ExpenseSheetController {

	def expenseSheetBusinessService
	
	
	def index(){
	    println "hey"
	}


    def registerUser(String userName, double initialBalanceAmount){
		
		def userModel = new User()
		render(view: "/user/create", model: [user: userModel])
		
		
        
    }
    
    def recordUserExpense(String userName, double amount){
    
    	expenseSheetBusinessService.recordUserExpense(userName,amount)
                                            
    }
    
    def listUserTransactions(String userName){
    	
    	def lstTransaction = expenseSheetBusinessService.listUserTransactions(userName)
    	
    	System.out.println("The size of the list is " + lstTransaction.size())
    	
    	session.lstTransaction
    	
        
    }
    
    def exportUserTransactionToCsv(String userName){
            
            
        def lstTransaction = expenseSheetBusinessService.listUserTransactions(userName)
        def newFile = new File("C:/csvFile/Transactions.csv")
        def ln = System.getProperty('line.separator')
        System.out.println("The size of the list is " + lstTransaction.size())
		def n = newFile.newWriter()
        
        for(def temp: lstTransaction){
            
            n << "$temp.date , $temp.spentAmount, $temp.balanceAmount $ln"                      
                                   
         }
         
         n.close()  
            
    }
 
}
