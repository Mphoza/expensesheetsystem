package co.za.expensesheet

class UrlMappings {


    static mappings = {
        // public pages
        "/"(view:"/layouts/main")
        //"/"(controller: "user", action: "index")
        "/create"(controller: "user", action: "create")
        "/save"(controller: "user", action: "save")
        "/show/$id"(controller: "User", action: "show")
        "/user/show/$id"(controller: "User", action: "show")
        "/user/edit/$id"(controller: "User", action: "edit")
        "/user/delete/$id"(controller: "User", action: "delete")
        "/user/update/$id"(controller: "User", action: "update")

		
		
        // protected pages
        //"/admin/$controller/$action?/$id?"()

        "500"(view:'/error')
    }
}
