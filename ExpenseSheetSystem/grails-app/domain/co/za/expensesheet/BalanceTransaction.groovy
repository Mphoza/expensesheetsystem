package co.za.expensesheet

class BalanceTransaction {


	User user
	Date date
	double spentAmount
	double balanceAmount

    static constraints = {
    
    	balance nullable: false
    	date nullable: false
    
    }

}
